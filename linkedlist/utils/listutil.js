function ListNode(val, next) {
    this.val = (val === undefined) ? 0 : val;
    this.next = (next === undefined) ? null : next;
}

function a2l(arr) {
    let head = null;
    let lobj = null;
    for(let item of arr){
        if(head === null){
            head = new ListNode(item);
            lobj = head;
        } else  {
            lobj.next = new ListNode(item);
            lobj = lobj.next;
        }
    }
    return head;
}

function l2a(lst)  {
    const arr = [];
    while(lst){
        arr.push(lst.val);
        lst = lst.next;
    }
    return arr;
}
module.exports =  {a2l, ListNode, l2a};