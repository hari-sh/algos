/*19. Remove Nth Node From End of List*/

/*
Given the head of a linked list, remove the nth node from the end of the list and return its head.
*/
 
/*
Example 1:
Input: head = [1,2,3,4,5], n = 2
Output: [1,2,3,5]
Example 2:

Input: head = [1], n = 1
Output: []
Example 3:

Input: head = [1,2], n = 1
Output: [1]
*/

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */

var removeNthFromEnd = function(head, n) {
    if(!head.next){
        head = null;
    }
    else{
        let n1 = head;
        let n2 = head;
        for(let count=0; count <= n; count++) {
            if (n2.next == null) {
                if (count == n - 1)
                    return  head.next;
            }
            n2 = n2.next;
        }
        while(n2){
            n1 = n1.next;
            n2 = n2.next;
        }
        if(n1.next){
            n1.next = n1.next.next;
        }
    }
    return head;
};

module.exports = {removeNthFromEnd};