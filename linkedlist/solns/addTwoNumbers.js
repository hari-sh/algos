/*'2. Add Two Numbers'*/
/*
You are given two non-empty linked lists representing two non-negative integers. 
The digits are stored in reverse order, and each of their nodes contains a single digit. 
Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.
*/

/* 
Example 1:
Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

Example 2:
Input: l1 = [0], l2 = [0]
Output: [0]

Example 3:
Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]
*/

/* Solution */

/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

const {ListNode} = require('../utils/listutil');

var addTwoNumbers = function(l1, l2) {
    let sum = l1.val + l2.val;
    let carry = Math.floor(sum/10);
    const l3 = new ListNode(sum%10);
    let nl1 = l1.next;
    let nl2 = l2.next;
    let nl3 = l3;
    while(nl1 || nl2 || carry){
        let v1 = 0;
        let v2 = 0;
        if(nl1){
            v1 = nl1.val;
            nl1 = nl1.next;
        }
        if(nl2){
            v2 = nl2.val;
            nl2 = nl2.next;
        }
        sum = v1 + v2 + carry;
        carry = Math.floor(sum/10);
        nl3.next = new ListNode(sum%10);
        nl3 = nl3.next;
    }
    return l3;
};

module.exports = {addTwoNumbers};