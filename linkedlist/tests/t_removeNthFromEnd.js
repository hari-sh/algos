const {a2l, l2a}= require('../utils/listutil.js')
const {removeNthFromEnd} = require('../solns/removeNthFromEnd');

function tcase(arr, pos, exp){
    let lst = a2l(arr);
    console.log('Case: ' + ++tcase.counter)
    console.log(arr);
    console.log(l2a(removeNthFromEnd(lst, pos)));
    console.log('Expected: ', exp);
    console.log('\n');
}

function testRemoveNthFromEnd()    {
    tcase.counter = 0;
    (() => tcase([1,2,3,4,5], 2, [1,2,3,5]))();
    (() => tcase([1,2], 1, [1]))();
    (() => tcase([1,2], 2, [2]))();
    (() => tcase([1], 1, []))();
}

module.exports = {testRemoveNthFromEnd};