// import {a2l,l2a} from '../solns/listutil.js';
// import { mergeTwoLists } from '../solns/mergeTwoLists.js';
const {mergeTwoLists} =  require('../solns/mergeTwoLists.js');
const {a2l, l2a}= require('../utils/listutil.js')

function tcase(a1, a2){
    let l1 = a2l(a1);
    let l2 = a2l(a2);
    console.log('Case: ' + ++tcase.counter)
    console.log(a1);
    console.log(a2);
    console.log(l2a(mergeTwoLists(l1, l2)));
    console.log('\n');
}

function testMergeTwoLists()    {
    tcase.counter = 0;
    (() => tcase([1,2,4], [1,3,4], [1,1,2,3,4,4]))();
    (() => tcase([], [], []))();
    (() => tcase([], [0], [0]))();
}

module.exports = {testMergeTwoLists}