const {addTwoNumbers} =  require('../solns/addTwoNumbers');
const {a2l, l2a} = require('../utils/listutil');

function tcase(a1, a2){
    let l1 = a2l(a1);
    let l2 = a2l(a2);
    console.log('Case: ' + ++tcase.counter)
    console.log(a1);
    console.log(a2);
    console.log(l2a(addTwoNumbers(l1, l2)));
    console.log('\n');
}

function testAddTwoNumbers()    {
    tcase.counter = 0;
    (() => tcase([2,4,3], [5,6,4]))();
    (() => tcase([0], [0]))();
    (() => tcase([9,9,9,9,9,9,9], [9,9,9,9]))();
}

module.exports = {testAddTwoNumbers};