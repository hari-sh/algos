const { testAddTwoNumbers } =  require("./tests/t_addTwoNumbers");
const { testRemoveNthFromEnd } = require("./tests/t_removeNthFromEnd");
const { testMergeTwoLists } = require("./tests/t_mergeTwoLists");
testAddTwoNumbers();
testRemoveNthFromEnd();
testMergeTwoLists();